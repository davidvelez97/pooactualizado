package facci.pm.ta2.poo.pra1;

import android.graphics.Bitmap;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.method.LinkMovementMethod;
import android.widget.ImageView;
import android.widget.TextView;

import facci.pm.ta2.poo.datalevel.DataException;
import facci.pm.ta2.poo.datalevel.DataObject;
import facci.pm.ta2.poo.datalevel.DataQuery;
import facci.pm.ta2.poo.datalevel.GetCallback;

public class DetailActivity extends AppCompatActivity {

    String dolar = "\u0024";
    TextView nombre, precio, descripcion;
    ImageView imageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);


        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle("PR1 :: Detail");

        nombre = (TextView)findViewById(R.id.TXTNombre);
        precio = (TextView)findViewById(R.id.TXTPrecio);
        descripcion = (TextView)findViewById(R.id.TXTDescripcion);

        imageView = (ImageView)findViewById(R.id.thumbnail);
        descripcion.setMovementMethod(LinkMovementMethod.getInstance());

        // INICIO - CODE6

        String object_id = getIntent().getStringExtra("object_id");

        DataQuery query = DataQuery.get("item");
        query.getInBackground(object_id , new GetCallback<DataObject>() {
                    @Override
                    public void done(DataObject object, DataException e) {

                        if (e == null){

                            nombre.setText((String) object.get("name"));
                            precio.setText((String) object.get("price") + dolar);
                            descripcion.setText((String) object.get("description"));
                            precio.setTextColor(Color.RED);
                            imageView.setImageBitmap((Bitmap) object.get("image"));

                        }else {

                        }

                    }

            });

        }
            // FIN - CODE6


}
